window.Transformer = Transformer = do ->

  TRANSFORM_ATTRIBUTES = [
    'translateX', 'translateY', 'translateZ',
    'rotateX', 'rotateY', 'rotateZ'
    'scale'
  ]

  _getAllProperties = (steps) ->
    props = []
    for step in steps
      for attr, value of step.object
        props.push(attr) if props.indexOf(attr) is -1
    return props

  _orderSteps = (steps) ->
    steps.sort (a, b) -> 
      if a.percent < b.percent then -1 else 1
    return steps

  _getUnits = (key) ->
    if key.indexOf("rotate") is 0 then "deg" 
    else if key.indexOf("scale") is 0 then ""
    else "px"

  _calcAttributeValue = (attribute, percent, steps) ->
    prev = null
    next = null
    loop_prev = null
    for step in steps when step.object[attribute]?
      if step.percent >= percent
        next = {percent: step.percent, value: step.object[attribute]}
        if loop_prev
          prev = {percent: loop_prev.percent, value: loop_prev.object[attribute]}
        else prev = next
        break
      loop_prev = step

    value = (next.value - prev.value) * (percent - prev.percent) / 
            (next.percent - prev.percent) + prev.value

    if isNaN(value) then 0 else value


  _applyCssToElement = (element, attributes) ->
    transforms = []
    for key, value of attributes
      if TRANSFORM_ATTRIBUTES.indexOf(key) is -1
        element.style[key] = value
      else
        transforms.push("#{key}(#{value}#{_getUnits(key)})")
    if transforms.length
      element.style.webkitTransform = transforms.join(" ")
      element.style.MozTransform = transforms.join(" ")


  class CSS

    constructor: (@el, options={}) ->
      @steps = []
      @initialized = false
      return @

    calcAttributes: (percent) ->
      @attributes = {}
      for property in @all_properties
        @attributes[property] = _calcAttributeValue(property, percent, @steps)
      @attributes

    init: ->
      @steps = _orderSteps(@steps)
      @all_properties = _getAllProperties(@steps)
      @initialized = true

    addStep: (percent, obj) ->
      @initialized = false
      @steps.push(percent: percent, object: obj)
      return @

    set: (percent, animate=false) ->
      if not @initialized then @init()
      @calcAttributes(percent)
      if @el then _applyCssToElement(@el, @attributes)
      return @

    applyTo: (element) ->
      if not @initialized then @init()
      _applyCssToElement(element, @attributes)


  (el) -> new CSS(el)




Book = do ->

  _bookContainer = ->
    el = document.createElement("article")
    el.className = "book"
    el

  BookClass = ->
    @container = null
    @pages = []
    @sides = []
    @index = -1
    return @

  BookClass:: =

    # addDoublePage: (html) ->
    #   len = @pages.length
    #   @sides.push(BookPage.createPageSide(html, true))
    #   @sides.push(BookPage.createPageSide(html, true, true))
    #   return @

    # _addPage: (front_side, back_side) ->
    #   @pages.push(BookPage.create(front_side, back_side)

    addPageSide: (html) ->
      @sides.push(BookPage.createPageSide(html))
      return @

    next: ->
      len =  @pages.length
      if len > @index + 1
        @index++
        @pages[@index].toggle(false)
        return true
      return false

    previous: ->
      if @index > -1
        @pages[@index].toggle(true)
        @index--
        return true
      return false

    render: (container) ->
      @element = _bookContainer()
      container.appendChild(@element)
      @num_pages = Math.round(@sides.length / 2)
      i = 0
      j = 0
      while i < @sides.length
        front = @sides[i]
        back = if i + 1 < @sides.length then @sides[i + 1] else null
        page = BookPage.create(@, front, back, j)
        page.render(@element)
        @pages.push(page)
        i += 2
        j++

      BookEvents.init(@)
      return @

    currentPage: ->
      @pages[@index]


  create: (options) ->
    new BookClass(options or {})


window.Book = Book


BookPage = do ->

  _bindToTransitionEnd = (el, callback) ->
    el.addEventListener("webkitTransitionEnd", callback)
    el.addEventListener("transitionend", callback)

  _unbindToTransitionEnd = (el, callback) ->
    el.removeEventListener("webkitTransitionEnd", callback)
    el.removeEventListener("transitionend", callback)

  _createPageContainer = (index, total_index) ->
    element = document.createElement("section")
    element.setAttribute("data-page-index", (index + 1) + "/" + total_index)
    element.className = "book-page"
    element.style.zIndex = index * -1
    element

  _onTransitionEnd = (ev) ->
    ev.currentTarget.removeAttribute("style")
    index_parts = ev.currentTarget.getAttribute("data-page-index").split("/")
    current = index_parts[0] - 1
    total = index_parts[1] - 1
    if ev.currentTarget.getAttribute("data-status")
      ev.currentTarget.style.zIndex = (total - current) * -1
    else ev.currentTarget.style.zIndex = current * -1
    # _unbindToTransitionEnd(ev.currentTarget, _onTransitionEnd)
    ev.preventDefault()
    ev.stopPropagation()

  PageClass = (@book, front_side, back_side, @index) ->
    @element = _createPageContainer(index, @book.num_pages)
    front_side.className = "book-page-front"
    @element.appendChild(front_side)
    back_side or= document.createElement("div")
    back_side.className = "book-page-back"
    @element.appendChild(back_side)
    _bindToTransitionEnd(@element, @_onTransitionEnd)
    # @forward_tr = _next_transform(@element)
    # @backward_tr = _prev_transform(@element)
    return @


  PageClass:: =

    _onTransitionEnd: (ev) =>
      @book._is_animating = false
      ev.currentTarget.removeAttribute("style")
      index_parts = ev.currentTarget.getAttribute("data-page-index").split("/")
      current = index_parts[0] - 1
      total = index_parts[1] - 1
      if ev.currentTarget.getAttribute("data-status")
        ev.currentTarget.style.zIndex = (total - current) * -1
      else ev.currentTarget.style.zIndex = current * -1
      # _unbindToTransitionEnd(ev.currentTarget, _onTransitionEnd)
      ev.preventDefault()
      ev.stopPropagation()

    toggle: (is_back=false) ->
      # _bindToTransitionEnd(@element, _onTransitionEnd)
      @book._is_animating = true
      @element.removeAttribute("style")
      @element.removeAttribute("book-page-noanim")
      @element.style.zIndex = 1
      if is_back is true
       @element.removeAttribute("data-status")
      else
        @element.setAttribute("data-status", "passed")
      return @

    render: (container) ->
      container.appendChild(@element)



  # create: (front, back, index, total_index) ->
  create: (book_ref, front, back, index) ->
    return new PageClass(book_ref, front, back, index)

  createPageSide: (html, is_double = false, scrolled_right = false) ->
    el = document.createElement("div")
    if is_double is true
      el.setAttribute("data-page-type", "double")
      subel = document.createElement("div")
      subel.innerHTML = html
      el.appendChild(subel)
      if scrolled_right is true
        el.scrollLeft = 999999
        subel.scrollLeft = 999999
      else
        el.scrollLeft = 0
        subel.scrollLeft = 0

    else
      el.innerHTML = html
    el



BookEvents = do ->

  TRIGGER_PX = 50
  RESISTANCE = 0.5


  _closestPage = (ev_target) ->
    if ev_target.className is "book-page"
      return ev_target
    else
      current = ev_target
      while current.parentNode
        if current.parentNode.className is "book-page"
          return current.parentNode
        current = current.parentNode
    return null

  _getEventX = (ev) ->
    ev.touches?[0].pageX or ev.pageX


  class BookEventsManager

    @next_transform = Transformer()
      .addStep(0,   {rotateY: 0,      opacity: 1})
      .addStep(100, {rotateY: -180,   opacity: 1})

    @prev_transform = Transformer()
      .addStep(0,   {rotateY: -180,   opacity: 1})
      .addStep(100, {rotateY: 0,      opacity: 1})

    constructor: (@instance) ->
      do @_initialize
      @instance.element.addEventListener "mousedown", @start
      @instance.element.addEventListener "mousemove", @move
      @instance.element.addEventListener "mouseup", @end
      @instance.element.addEventListener "touchstart", @start
      @instance.element.addEventListener "touchmove", @move
      @instance.element.addEventListener "touchend", @end
      return @

    _initialize: ->
      @started = false
      @diffX = 0
      @startX = 0
      @animating = false
      @page = null

    start: (ev) =>
      if @instance._is_animating is true then return
      if @started is true
        @end(true)
        return

      @started = true
      @diffX = 0
      @startX = _getEventX(ev)
      @current = _closestPage(ev.target)
      ev.preventDefault()
      ev.stopPropagation()

    move: (ev) =>
      if @instance._is_animating is true then return
      if @started and @current
        @current.style.zIndex = 1
        @diffX = _getEventX(ev) - @startX
        percent = Math.abs(Math.max(Math.min(@diffX * 20 / TRIGGER_PX, 99), -99))
        @current.setAttribute("book-page-noanim", "true")
        if @diffX < 0 and !@current.getAttribute("data-status")
          @constructor.next_transform.set(percent).applyTo(@current)
        else if @diffX > 0 and !!@current.getAttribute("data-status") is true
          @constructor.prev_transform.set(percent).applyTo(@current)

        ev.preventDefault()
        ev.stopPropagation()

    end: (ev, abort=false) =>
      if @instance._is_animating is true then return
      if Math.abs(@diffX) > TRIGGER_PX and abort is false
        command = if @diffX > 0 then "previous" else "next"
        @instance[command]()
      else if @current or abort is true
        @current.removeAttribute("book-page-noanim")
        @current.removeAttribute("style")

      do @_initialize


  init = (book_instance) ->
    new BookEventsManager(book_instance)
    return true


  init: init
