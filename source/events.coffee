BookEvents = do ->

  TRIGGER_PX = 50
  RESISTANCE = 0.5


  _closestPage = (ev_target) ->
    if ev_target.className is "book-page"
      return ev_target
    else
      current = ev_target
      while current.parentNode
        if current.parentNode.className is "book-page"
          return current.parentNode
        current = current.parentNode
    return null

  _getEventX = (ev) ->
    ev.touches?[0].pageX or ev.pageX


  class BookEventsManager

    @next_transform = Transformer()
      .addStep(0,   {rotateY: 0,      opacity: 1})
      .addStep(100, {rotateY: -180,   opacity: 1})

    @prev_transform = Transformer()
      .addStep(0,   {rotateY: -180,   opacity: 1})
      .addStep(100, {rotateY: 0,      opacity: 1})

    constructor: (@instance) ->
      do @_initialize
      @instance.element.addEventListener "mousedown", @start
      @instance.element.addEventListener "mousemove", @move
      @instance.element.addEventListener "mouseup", @end
      @instance.element.addEventListener "touchstart", @start
      @instance.element.addEventListener "touchmove", @move
      @instance.element.addEventListener "touchend", @end
      return @

    _initialize: ->
      @started = false
      @diffX = 0
      @startX = 0
      @animating = false
      @page = null

    start: (ev) =>
      if @instance._is_animating is true then return
      if @started is true
        @end(true)
        return

      @started = true
      @diffX = 0
      @startX = _getEventX(ev)
      @current = _closestPage(ev.target)
      ev.preventDefault()
      ev.stopPropagation()

    move: (ev) =>
      if @instance._is_animating is true then return
      if @started and @current
        @current.style.zIndex = 1
        @diffX = _getEventX(ev) - @startX
        percent = Math.abs(Math.max(Math.min(@diffX * 20 / TRIGGER_PX, 99), -99))
        @current.setAttribute("book-page-noanim", "true")
        if @diffX < 0 and !@current.getAttribute("data-status")
          @constructor.next_transform.set(percent).applyTo(@current)
        else if @diffX > 0 and !!@current.getAttribute("data-status") is true
          @constructor.prev_transform.set(percent).applyTo(@current)

        ev.preventDefault()
        ev.stopPropagation()

    end: (ev, abort=false) =>
      if @instance._is_animating is true then return
      if Math.abs(@diffX) > TRIGGER_PX and abort is false
        command = if @diffX > 0 then "previous" else "next"
        @instance[command]()
      else if @current or abort is true
        @current.removeAttribute("book-page-noanim")
        @current.removeAttribute("style")

      do @_initialize


  init = (book_instance) ->
    new BookEventsManager(book_instance)
    return true


  init: init
