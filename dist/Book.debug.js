(function() {
  var Book, BookEvents, BookPage, Transformer,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Transformer = Transformer = (function() {
    var CSS, TRANSFORM_ATTRIBUTES, _applyCssToElement, _calcAttributeValue, _getAllProperties, _getUnits, _orderSteps;
    TRANSFORM_ATTRIBUTES = ['translateX', 'translateY', 'translateZ', 'rotateX', 'rotateY', 'rotateZ', 'scale'];
    _getAllProperties = function(steps) {
      var attr, props, step, value, _i, _len, _ref;
      props = [];
      for (_i = 0, _len = steps.length; _i < _len; _i++) {
        step = steps[_i];
        _ref = step.object;
        for (attr in _ref) {
          value = _ref[attr];
          if (props.indexOf(attr) === -1) {
            props.push(attr);
          }
        }
      }
      return props;
    };
    _orderSteps = function(steps) {
      steps.sort(function(a, b) {
        if (a.percent < b.percent) {
          return -1;
        } else {
          return 1;
        }
      });
      return steps;
    };
    _getUnits = function(key) {
      if (key.indexOf("rotate") === 0) {
        return "deg";
      } else if (key.indexOf("scale") === 0) {
        return "";
      } else {
        return "px";
      }
    };
    _calcAttributeValue = function(attribute, percent, steps) {
      var loop_prev, next, prev, step, value, _i, _len;
      prev = null;
      next = null;
      loop_prev = null;
      for (_i = 0, _len = steps.length; _i < _len; _i++) {
        step = steps[_i];
        if (!(step.object[attribute] != null)) {
          continue;
        }
        if (step.percent >= percent) {
          next = {
            percent: step.percent,
            value: step.object[attribute]
          };
          if (loop_prev) {
            prev = {
              percent: loop_prev.percent,
              value: loop_prev.object[attribute]
            };
          } else {
            prev = next;
          }
          break;
        }
        loop_prev = step;
      }
      value = (next.value - prev.value) * (percent - prev.percent) / (next.percent - prev.percent) + prev.value;
      if (isNaN(value)) {
        return 0;
      } else {
        return value;
      }
    };
    _applyCssToElement = function(element, attributes) {
      var key, transforms, value;
      transforms = [];
      for (key in attributes) {
        value = attributes[key];
        if (TRANSFORM_ATTRIBUTES.indexOf(key) === -1) {
          element.style[key] = value;
        } else {
          transforms.push("" + key + "(" + value + (_getUnits(key)) + ")");
        }
      }
      if (transforms.length) {
        element.style.webkitTransform = transforms.join(" ");
        return element.style.MozTransform = transforms.join(" ");
      }
    };
    CSS = (function() {
      function CSS(el, options) {
        this.el = el;
        if (options == null) {
          options = {};
        }
        this.steps = [];
        this.initialized = false;
        return this;
      }

      CSS.prototype.calcAttributes = function(percent) {
        var property, _i, _len, _ref;
        this.attributes = {};
        _ref = this.all_properties;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          property = _ref[_i];
          this.attributes[property] = _calcAttributeValue(property, percent, this.steps);
        }
        return this.attributes;
      };

      CSS.prototype.init = function() {
        this.steps = _orderSteps(this.steps);
        this.all_properties = _getAllProperties(this.steps);
        return this.initialized = true;
      };

      CSS.prototype.addStep = function(percent, obj) {
        this.initialized = false;
        this.steps.push({
          percent: percent,
          object: obj
        });
        return this;
      };

      CSS.prototype.set = function(percent, animate) {
        if (animate == null) {
          animate = false;
        }
        if (!this.initialized) {
          this.init();
        }
        this.calcAttributes(percent);
        if (this.el) {
          _applyCssToElement(this.el, this.attributes);
        }
        return this;
      };

      CSS.prototype.applyTo = function(element) {
        if (!this.initialized) {
          this.init();
        }
        return _applyCssToElement(element, this.attributes);
      };

      return CSS;

    })();
    return function(el) {
      return new CSS(el);
    };
  })();

  Book = (function() {
    var BookClass, _bookContainer;
    _bookContainer = function() {
      var el;
      el = document.createElement("article");
      el.className = "book";
      return el;
    };
    BookClass = function() {
      this.container = null;
      this.pages = [];
      this.sides = [];
      this.index = -1;
      return this;
    };
    BookClass.prototype = {
      addPageSide: function(html) {
        this.sides.push(BookPage.createPageSide(html));
        return this;
      },
      next: function() {
        var len;
        len = this.pages.length;
        if (len > this.index + 1) {
          this.index++;
          this.pages[this.index].toggle(false);
          return true;
        }
        return false;
      },
      previous: function() {
        if (this.index > -1) {
          this.pages[this.index].toggle(true);
          this.index--;
          return true;
        }
        return false;
      },
      render: function(container) {
        var back, front, i, j, page;
        this.element = _bookContainer();
        container.appendChild(this.element);
        this.num_pages = Math.round(this.sides.length / 2);
        i = 0;
        j = 0;
        while (i < this.sides.length) {
          front = this.sides[i];
          back = i + 1 < this.sides.length ? this.sides[i + 1] : null;
          page = BookPage.create(this, front, back, j);
          page.render(this.element);
          this.pages.push(page);
          i += 2;
          j++;
        }
        BookEvents.init(this);
        return this;
      },
      currentPage: function() {
        return this.pages[this.index];
      }
    };
    return {
      create: function(options) {
        return new BookClass(options || {});
      }
    };
  })();

  window.Book = Book;

  BookPage = (function() {
    var PageClass, _bindToTransitionEnd, _createPageContainer, _onTransitionEnd, _unbindToTransitionEnd;
    _bindToTransitionEnd = function(el, callback) {
      el.addEventListener("webkitTransitionEnd", callback);
      return el.addEventListener("transitionend", callback);
    };
    _unbindToTransitionEnd = function(el, callback) {
      el.removeEventListener("webkitTransitionEnd", callback);
      return el.removeEventListener("transitionend", callback);
    };
    _createPageContainer = function(index, total_index) {
      var element;
      element = document.createElement("section");
      element.setAttribute("data-page-index", (index + 1) + "/" + total_index);
      element.className = "book-page";
      element.style.zIndex = index * -1;
      return element;
    };
    _onTransitionEnd = function(ev) {
      var current, index_parts, total;
      ev.currentTarget.removeAttribute("style");
      index_parts = ev.currentTarget.getAttribute("data-page-index").split("/");
      current = index_parts[0] - 1;
      total = index_parts[1] - 1;
      if (ev.currentTarget.getAttribute("data-status")) {
        ev.currentTarget.style.zIndex = (total - current) * -1;
      } else {
        ev.currentTarget.style.zIndex = current * -1;
      }
      ev.preventDefault();
      return ev.stopPropagation();
    };
    PageClass = function(book, front_side, back_side, index) {
      this.book = book;
      this.index = index;
      this.element = _createPageContainer(index, this.book.num_pages);
      front_side.className = "book-page-front";
      this.element.appendChild(front_side);
      back_side || (back_side = document.createElement("div"));
      back_side.className = "book-page-back";
      this.element.appendChild(back_side);
      _bindToTransitionEnd(this.element, this._onTransitionEnd);
      return this;
    };
    PageClass.prototype = {
      _onTransitionEnd: (function(_this) {
        return function(ev) {
          var current, index_parts, total;
          _this.book._is_animating = false;
          ev.currentTarget.removeAttribute("style");
          index_parts = ev.currentTarget.getAttribute("data-page-index").split("/");
          current = index_parts[0] - 1;
          total = index_parts[1] - 1;
          if (ev.currentTarget.getAttribute("data-status")) {
            ev.currentTarget.style.zIndex = (total - current) * -1;
          } else {
            ev.currentTarget.style.zIndex = current * -1;
          }
          ev.preventDefault();
          return ev.stopPropagation();
        };
      })(this),
      toggle: function(is_back) {
        if (is_back == null) {
          is_back = false;
        }
        this.book._is_animating = true;
        this.element.removeAttribute("style");
        this.element.removeAttribute("book-page-noanim");
        this.element.style.zIndex = 1;
        if (is_back === true) {
          this.element.removeAttribute("data-status");
        } else {
          this.element.setAttribute("data-status", "passed");
        }
        return this;
      },
      render: function(container) {
        return container.appendChild(this.element);
      }
    };
    return {
      create: function(book_ref, front, back, index) {
        return new PageClass(book_ref, front, back, index);
      },
      createPageSide: function(html, is_double, scrolled_right) {
        var el, subel;
        if (is_double == null) {
          is_double = false;
        }
        if (scrolled_right == null) {
          scrolled_right = false;
        }
        el = document.createElement("div");
        if (is_double === true) {
          el.setAttribute("data-page-type", "double");
          subel = document.createElement("div");
          subel.innerHTML = html;
          el.appendChild(subel);
          if (scrolled_right === true) {
            el.scrollLeft = 999999;
            subel.scrollLeft = 999999;
          } else {
            el.scrollLeft = 0;
            subel.scrollLeft = 0;
          }
        } else {
          el.innerHTML = html;
        }
        return el;
      }
    };
  })();

  BookEvents = (function() {
    var BookEventsManager, RESISTANCE, TRIGGER_PX, init, _closestPage, _getEventX;
    TRIGGER_PX = 50;
    RESISTANCE = 0.5;
    _closestPage = function(ev_target) {
      var current;
      if (ev_target.className === "book-page") {
        return ev_target;
      } else {
        current = ev_target;
        while (current.parentNode) {
          if (current.parentNode.className === "book-page") {
            return current.parentNode;
          }
          current = current.parentNode;
        }
      }
      return null;
    };
    _getEventX = function(ev) {
      var _ref;
      return ((_ref = ev.touches) != null ? _ref[0].pageX : void 0) || ev.pageX;
    };
    BookEventsManager = (function() {
      BookEventsManager.next_transform = Transformer().addStep(0, {
        rotateY: 0,
        opacity: 1
      }).addStep(100, {
        rotateY: -180,
        opacity: 1
      });

      BookEventsManager.prev_transform = Transformer().addStep(0, {
        rotateY: -180,
        opacity: 1
      }).addStep(100, {
        rotateY: 0,
        opacity: 1
      });

      function BookEventsManager(instance) {
        this.instance = instance;
        this.end = __bind(this.end, this);
        this.move = __bind(this.move, this);
        this.start = __bind(this.start, this);
        this._initialize();
        this.instance.element.addEventListener("mousedown", this.start);
        this.instance.element.addEventListener("mousemove", this.move);
        this.instance.element.addEventListener("mouseup", this.end);
        this.instance.element.addEventListener("touchstart", this.start);
        this.instance.element.addEventListener("touchmove", this.move);
        this.instance.element.addEventListener("touchend", this.end);
        return this;
      }

      BookEventsManager.prototype._initialize = function() {
        this.started = false;
        this.diffX = 0;
        this.startX = 0;
        this.animating = false;
        return this.page = null;
      };

      BookEventsManager.prototype.start = function(ev) {
        if (this.instance._is_animating === true) {
          return;
        }
        if (this.started === true) {
          this.end(true);
          return;
        }
        this.started = true;
        this.diffX = 0;
        this.startX = _getEventX(ev);
        this.current = _closestPage(ev.target);
        ev.preventDefault();
        return ev.stopPropagation();
      };

      BookEventsManager.prototype.move = function(ev) {
        var percent;
        if (this.instance._is_animating === true) {
          return;
        }
        if (this.started && this.current) {
          this.current.style.zIndex = 1;
          this.diffX = _getEventX(ev) - this.startX;
          percent = Math.abs(Math.max(Math.min(this.diffX * 20 / TRIGGER_PX, 99), -99));
          this.current.setAttribute("book-page-noanim", "true");
          if (this.diffX < 0 && !this.current.getAttribute("data-status")) {
            this.constructor.next_transform.set(percent).applyTo(this.current);
          } else if (this.diffX > 0 && !!this.current.getAttribute("data-status") === true) {
            this.constructor.prev_transform.set(percent).applyTo(this.current);
          }
          ev.preventDefault();
          return ev.stopPropagation();
        }
      };

      BookEventsManager.prototype.end = function(ev, abort) {
        var command;
        if (abort == null) {
          abort = false;
        }
        if (this.instance._is_animating === true) {
          return;
        }
        if (Math.abs(this.diffX) > TRIGGER_PX && abort === false) {
          command = this.diffX > 0 ? "previous" : "next";
          this.instance[command]();
        } else if (this.current || abort === true) {
          this.current.removeAttribute("book-page-noanim");
          this.current.removeAttribute("style");
        }
        return this._initialize();
      };

      return BookEventsManager;

    })();
    init = function(book_instance) {
      new BookEventsManager(book_instance);
      return true;
    };
    return {
      init: init
    };
  })();

}).call(this);
